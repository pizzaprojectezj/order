//Final Project - CS120
//Zhen Guo
//Jordan Kirsch
//Ethan Meyers
//  Restaurant.h
//  FP-restaurant
//
//  Created by Jordan on 11/28/18.
//  Copyright � 2018 Jordan. All rights reserved.
//
#
#ifndef Restaurant_h
#define Restaurant_h

#include <stdexcept>
#include <iostream>
#include <list>
#include <string>
#include <cstring>
#include "Driver.h"

class Restaurant {
    public:
    
    //Pre: None
    //Post: Creates a restaurant in an initial state, with no drivers or orders.
    Restaurant();
    
    //Pre: None
    //Post: Prints status of orders and logged-in drivers, as specified by the �status� command description.
    void status();
    
    //Pre: None
    //Post: Prints summary statistics on orders and drivers
    void summary();
    
    //Pre: None
    //Post: If a driver with the given name exists within the system (logged in or not), returns a pointer to that driver. Otherwise, returns a null pointer.
    Driver* getDriver(string name);
    
    //Pre: None
    //Post: Adds the given driver to the system.
    void addDriver(Driver *driver);
    
    //Pre: None
    //Post: Adds the given order to the system, enqueuing it for cooking.
    void addOrder(Order *order);
    
    //Pre: The cooking queue is not empty.
    //Post: Removes the oldest order from the cooking queue and enqueues it for departure.
    void serveNextOrder() throw (logic_error);
    
    //Pre: The departure queue is not empty.
    //Post: Removes the oldest order from the departure queue and returns it.
    Order *departNextOrder() throw (logic_error);
    
    //Pre: None
    //Post: The order carried by the driver is delivered at the given time. The driver receives the given tip.
    void deliver(Driver *driver, Time time, float tip);
    
private:
    
    //Current private members, may change while writing fucntions
    //uses STL list and queue
    list<Driver> drivers;
    list<Order> cooking;
    int completedOrders;
    list<Order> departure;
    int totalTime;
};
#endif /* Restaurant_h */

