//Zhen Guo
//Jordan Kirsch
//Ethan Meyers

#include <stdio.h>
#include <iostream>
#include <string>
#include "Order.h"
#include <stdexcept>

using namespace std;

/* Constructor
 Post: Creates an order with default values for data members */
Order::Order()
{
    location = " ";
    placedOrder = Time(0,0);
    deliveredOrder = Time(0,0);
    isAtStore = 0;
    isDelivered = 0;
    onTheWay = 0;
    
}


/* Order
 Post: Creates an order with the given order time and information */
Order::Order(Time time, string info)
{
    placedOrder = time;
    location = info;
    isAtStore = 1;
}


/* depart
 Pre: Order is at the restaurant
 Post: Order is out for delivery */
void Order::depart() throw (logic_error)
{
    if(isAtStore != 1)
        throw logic_error("Order is not at the store");
    
    isAtStore = 0;
    onTheWay = 1;
    isDelivered = 0;
}


/* deliver
 Pre: Order is out for delivery
 Post: Order is delivered. Time to delivery is recorded */
void Order::deliver(Time time) throw (logic_error)
{
    
    if(onTheWay != 1)
        throw logic_error("Order has left the store");
    isAtStore = 0;
    onTheWay = 0;
    isDelivered = 1;
    deliveredOrder = time;
    
}


/* getMinToDelivery
 Pre: Order is delivered
 Post: Returns the minutes until the order is delivered (i.e., between "order" and "deliver" commands */
int Order::getMinToDelivery() throw (logic_error)
{
    if(isDelivered == 0)
        throw logic_error("Order was not delivered");
    
    return placedOrder.elapsedMin(placedOrder, deliveredOrder);
    
}


/* toString
 Post: Returns a string containing the order time and info */
string Order::toString()
{
    
    return placedOrder.toString() + location;
    
}

















