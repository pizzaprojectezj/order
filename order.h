#ifndef ORDER_H
#define ORDER_H

#include <iostream>
#include <string>
#include <ctime>
#include <queue>
#include "Time.h"

using namespace std;

class Order
{
    
public:
    
    /* Constructor
     Post: Creates an order with default values for data members */
    Order();
    
    /* Order
     Post: Creates an order with the given order time and information */
    Order(Time time, string info);
    
    /* depart
     Pre: Order is at the restaurant
     Post: Order is out for delivery */
    void depart() throw (logic_error);
    
    /* deliver
     Pre: Order is out for delivery
     Post: Order is delivered. Time to delivery is recorded */
    void deliver(Time time) throw (logic_error);
    
    /* getMinToDelivery
     Pre: Order is delivered
     Post: Returns the minutes until the order is delivered (i.e., between "order" and "deliver" commands */
    int getMinToDelivery() throw (logic_error);
    
    /* toString
     Post: Returns a string containing the order time and info */
    string toString();
    
private:
    
    /* Private data members, may change while writing functions */
    string location;
    Time placedOrder;
    Time deliveredOrder;
    bool isAtStore;
    bool onTheWay;
    bool isDelivered;
    
};

#endif

