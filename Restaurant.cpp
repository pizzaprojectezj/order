//
//  Restaurant.cpp
//  FP-restaurant
//
//  Created by Jordan on 11/28/18.
//  Copyright � 2018 Jordan. All rights reserved.
//

#include <stdio.h>
#include "Restaurant.h"


//Pre: None
//Post: Creates a restaurant in an initial state, with no drivers or orders.
Restaurant::Restaurant(): totalTime(0), completedOrders(0)
{}

//Pre: None
//Post: Prints status of orders and logged-in drivers, as specified by the �status� command description.
void Restaurant ::status()
{
    cout << "Orders waiting to cook: " << endl;
    list<Order> :: iterator it;
    list<Driver> :: iterator d;
    for(it = cooking.begin(); it != cooking.end(); it++)
    {
        cout << it->toString() << endl;
    }
    cout << "Orders waiting to depart: " << endl;
    for(it = departure.begin(); it != departure.end(); it++)
    {
        cout << it->toString() << endl;
    }
    cout << "Drivers: " << endl;
    for (d = drivers.begin(); d != drivers.end(); d++)
    {
        if(d->isLoggedIn())
        {
            cout << d->toString() << endl;
        }
    }
}

//Pre: None
//Post: Prints summary statistics on orders and drivers
void Restaurant:: summary()
{
    cout << "Number of orders completed: " << completedOrders << endl;
    cout << "Average time per order: " << (double)(totalTime/completedOrders) << endl;
    list<Driver> :: iterator d;
    for (d = drivers.begin(); d != drivers.end(); d++)
    {
        cout << "Driver" << d->getName() << ":" << endl;
        cout << "Number of deliveries completed: " << d->getTotalDeliveries() << endl;
        if(d->getTotalDeliveries() == 0)
        {
            cout << "Average time per delivery : N/A" << endl;
        }
        else
        {
            cout <<"Average time per delivery: " << (double)(d->getTotalMinDelivering()/d->getTotalDeliveries()) << endl;
        }
        cout << "Total driving time: " << d->getTotalMinDriving() << endl;
        cout << "Total tips: " << d->getTotalTips() << endl;
    }
    
    
}

//Pre: None
//Post: If a driver with the given name exists within the system (logged in or not), returns a pointer to that driver. Otherwise, returns a null pointer.
Driver* Restaurant :: getDriver(string name)
{
    list<Driver> :: iterator it;
    for(it = drivers.begin(); it != drivers.end(); it++)
    {
        if(it->getName() == name)
        {
            return &*it;
        }
    }
    return nullptr;
}

//Pre: None
//Post: Adds the given driver to the system.
void Restaurant :: addDriver(Driver *driver)
{
    drivers.push_front(*driver);
}

//Pre: None
//Post: Adds the given order to the system, enqueuing it for cooking.
void Restaurant :: addOrder(Order *order)
{
    cooking.push_front(*order);
}

//Pre: The cooking queue is not empty.
//Post: Removes the oldest order from the cooking queue and enqueues it for departure.
void Restaurant :: serveNextOrder() throw (logic_error)
{
    if(cooking.size() == 0)
        __throw_logic_error("***There are no oders to serve***");
    
    else
    {
        departure.push_front(cooking.back());
        cooking.pop_back();
    }
}

//Pre: The departure queue is not empty.
//Post: Removes the oldest order from the departure queue and returns it.
Order* Restaurant :: departNextOrder() throw (logic_error)
{
    if(departure.size() == 0)
        __throw_logic_error("***No orders are ready to depart***");
    
    else
    {
        return &departure.back();
    }
}

//Pre: None
//Post: The order carried by the driver is delivered at the given time. The driver receives the given tip.
void Restaurant :: deliver(Driver *driver, Time time, float tip)
{
    driver->deliver(time, tip);
    Order temp = driver->getOrder();
    totalTime = totalTime + temp.getMinToDelivery();
    completedOrders = completedOrders + 1;
}

