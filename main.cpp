//Final Project - CS120
//Zhen Guo
//Jordan Kirsch
//Ethan Meyers
//  main.cpp
//  FP-restaurant
//
//  Created by Zhen Guo on 12/05/18.
//  Copyright � 2018 Zhen. All rights reserved.
//
#include <string>
#include <sstream>
#include <vector>
#include <iostream>

using namespace std;

#include "Restaurant.h"

Restaurant D;

void login(string name)
{
    if(!D.getDriver(name))
    {
        Driver driver = Driver(name);
        D.addDriver(&driver);
    }
    
    else
        D.getDriver(name)->login();
}


void logout(string name)
{
    D.getDriver(name)->logout();
}

void order(Time time, string info)
{
    Order o = Order(time,info);
    D.addOrder(&o);
}

void serve()
{
    D.serveNextOrder();
}

void depart(Time time, string driver)
{
    D.getDriver(driver)->depart(time,*D.departNextOrder());
}

void deliver(Time time, string driver, float tip)
{
    D.deliver(D.getDriver(driver), time, tip);
}

void arrive(Time time, string driver)
{
    D.getDriver(driver)->arrive(time);
}

void status()
{
    D.status();
}

void summary()
{
    D.summary();
}

vector<string> time_tokenizer(const string token)
{
    stringstream time(token);
    time << token;
    string time_token;
    vector<string> time_tokens;
    while (std::getline(time, time_token, ':'))
    {
        time_tokens.push_back(time_token);
    }
    
    return time_tokens;
}

void print_help()
{
    cout << endl << "Commands:" << endl;
    cout << "  H       : Help (displays this message)" << endl;
    cout << "  login   : login DRIVER" << endl;
    cout << "  logout  : logout Driver" << endl;
    cout << "  order   : order HH:MM INFO " << endl;
    cout << "  serve   : serve HH:MM order ready to go" << endl;
    cout << "  depart  : depart HH:MM DRIVER " << endl;
    cout << "  deliver : deliver HH:MM DRIVER TIP" << endl;
    cout << "  arrive  : arrive HH:MM DRIVER " << endl;
    cout << "  status  : Status" << endl;
    cout << "  summary : Summary" << endl;
    cout << "  Q       : Quit the test program" << endl;
    cout << endl;
}


void test_Restaurant(Restaurant & R) {
    
    string test_string;
    vector <string> tokens;    // Vector of string to save tokens
    int H;
    int M;
    string name;
    string info;
    string cmd;
    
    print_help();
    
    do {
        tokens.clear();
        
        cout << endl << "Command: ";                  // Read command
        getline (cin, test_string);
        
        stringstream tokenStream(test_string);
        string intermediate;
        
        // Tokenizing w.r.t. space ' '
        while(getline(tokenStream, intermediate, ' '))
        {
            tokens.push_back(intermediate);
        }
        
        cmd = tokens.front();
        
        try {
            
            if (cmd == "H" || cmd == "h") {
                print_help();
               
            }
            
            else if (cmd == "login") {
                cout << "Login " << tokens[1]<< endl;
                login(tokens[1]);
                status();
                
            }
            
            else if (cmd == "order" ) {
                
                auto time_tokens = time_tokenizer(tokens[1]);
                H = stoi(time_tokens[0]);
                M = stoi(time_tokens[1]);
                Time time = Time(H, M);
                
                vector<string>::iterator ptr;
                for(ptr = tokens.begin() + 2; ptr != tokens.end(); ptr++)
                {
                    info.append(" "+ *ptr);
                }
                
                order(time, info);
                status();
            }
            
            else if (cmd == "serve" ) {
                
                serve();
                cout << "Serve at" << tokens[1] << endl;
                status();
               
            }
            
            else if (cmd == "depart" ) {
                
                auto time_tokens = time_tokenizer(tokens[1]);
                H = stoi(time_tokens[0]);
                M = stoi(time_tokens[1]);
                Time time = Time(H, M);
                
                depart(time, tokens[2]);
                
                cout << "depart " << tokens[1] << tokens[2] << endl;
                status();
              
            }
            else if(cmd == "deliver")
            {
                auto time_tokens = time_tokenizer(tokens[1]);
                H = stoi(time_tokens[0]);
                M = stoi(time_tokens[1]);
                Time time = Time(H,M);
                int tip = stoi(tokens[3]);
                
                deliver(time, tokens[2], tip);
            }
            else if (cmd == "arrive")
            {
                auto time_tokens = time_tokenizer(tokens[1]);
                H = stoi(time_tokens[0]);
                M = stoi(time_tokens[1]);
                Time time = Time(H,M);
                
                arrive(time, tokens[2]);
            
            }
            
            else if (cmd == "status")
            {
                status();
            
            }
            else if (cmd == "summary")
            {
                summary();
             
            }
            else if (cmd == "Q" || cmd == "q" )                   // Quit test program
                break;
            
            
        }
        catch (logic_error e) {
            cout << "Error: " << e.what() << endl;
        }
        
    }while (cmd != "Q" && cmd != "q");
}

int main()
{
    
    cout << "Testing array implementation" << endl;
    
    test_Restaurant(D);
    
}
